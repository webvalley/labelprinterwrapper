<?php

define('LABELPRINTER_PATH', dirname(__DIR__) . '/src/LabelPrinter/');

require_once LABELPRINTER_PATH . 'LabelPrinter.php';

require_once LABELPRINTER_PATH . '/Model/Shipment/Parcel.php';
require_once LABELPRINTER_PATH . '/Model/Shipment/PreferredSender.php';
require_once LABELPRINTER_PATH . '/Model/Shipment/Recipient.php';
require_once LABELPRINTER_PATH . '/Model/Shipment/Shipment.php';

require_once LABELPRINTER_PATH . '/Model/Shipper/Service.php';
require_once LABELPRINTER_PATH . '/Model/Shipper/Shipper.php';
