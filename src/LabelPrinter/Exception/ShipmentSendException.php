<?php

namespace LabelPrinter\Exception;

class ShipmentSendException extends LabelPrintException
{
    private $errors;

    /**
     * ShipmentSendException constructor.
     * @param $errors
     */
    public function __construct(array $errors)
    {
        parent::__construct(print_r($errors, true));

        $this->errors = $errors;
    }

    /**
     * @return array
     */
    public function getErrors() : array
    {
        return $this->errors;
    }
}