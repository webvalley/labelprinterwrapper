<?php
namespace LabelPrinter;

use LabelPrinter\Exception\ShipmentSendException;
use LabelPrinter\Model\SearchCriteria;
use LabelPrinter\Model\Shipment\Shipment;
use LabelPrinter\Model\Shipper\Service;
use LabelPrinter\Model\Shipper\Shipper;

class LabelPrinter
{
    private $apiUrl;
    private $clientId;
    private $clientSecret;

    private $baseLabelPrintterUrl;

    private $accessToken;
    private $accessTokenType;
    private $tokenExpire;

    /**
     * LabelPrinter constructor.
     */
    public function __construct(string $apiUrl, string $clientId, string $clientSecret)
    {
        $this->apiUrl = $apiUrl;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;

        $this->baseLabelPrintterUrl = $apiUrl;
    }

    private function setToken(string $accessToken, string $tokenType = 'Bearer')
    {
        $this->accessToken = $accessToken;
        $this->accessTokenType = $tokenType;
        $this->tokenExpire = null;
    }

    public function authorize()
    {
        $authResponse = $this->request('POST', '/connect/token',
            array('grant_type' => 'client_credentials','scope' => 'lp_api_full'),
            array(
                'Authorization' => 'Basic ' . base64_encode($this->clientId . ':' . $this->clientSecret)));

        if(isset($authResponse['access_token'], $authResponse['token_type'])){
            $this->accessToken = $authResponse['access_token'];
            $this->accessTokenType = $authResponse['token_type'];
            // todo handle token expire
        }else{
            // todo handle missing auth params/failed auth process
        }
    }

    public function searchShipmentOrders(SearchCriteria $searchCriteria) : array
    {
        $searchShipmentResponse = $this->requestJson('POST', '/shipments/search', $searchCriteria->serialize());

        if($searchShipmentResponse['code'] === 200){
            var_dump($searchShipmentResponse);
            die();
        }
    }

    public function getShipmentState($id) : int
    {
        $shippmentStateResponse = $this->requestJson('GET', "/shipments/{$id}/state");

        if($shippmentStateResponse['code'] === 200){
            // TODO check if more parcels could have more codes..
            return $shippmentStateResponse['data'][0]['code'];
        }

        // todo handle error codes different than 200
    }

    public function getShippers() : array
    {
        $shippersResponse = $this->requestJson('GET', '/shippers');
        $shippers = array();

        foreach($shippersResponse['data'] as $shipper)
        {
            $services = array();
            foreach($shipper['services'] as $service)
            {
                $services[] = new Service($service['code'], $service['name']);
            }

            $shippers[] = new Shipper($shipper['code'], $shipper['name'], $shipper['phone'], $shipper['email'], $services);
        }

        return $shippers;
    }

    public function findShipmentById($id) : Shipment
    {
        $shipmentResponse = $this->requestJson('GET', "/shipments/{$id}");

        if($shipmentResponse['code'] === 200){
            return (new Shipment($this))->deserialize($shipmentResponse['data']);
        }

        // TODO handle not found
    }

    public function cancelShipment($id) : bool
    {
        $cancelResponse = $this->requestJson('DELETE', "/shipments/{$id}");

        if($cancelResponse['code'] === 200){
            return true;
        }
    }

    public function sendShipment(Shipment $shipment) : bool
    {
        $sendShipmentResponse = $this->requestJson('POST', '/shipments', $shipment->serialize());

        if($sendShipmentResponse['code'] === 201){
            $shipment->deserialize($sendShipmentResponse['data']);

            return true;
        }

        throw new ShipmentSendException($sendShipmentResponse['errors']);
    }

    public function createShipment()
    {
        return new Shipment($this);
    }

    public function requestJson(string $method,string $url, $data = null, $authorized = true) : array
    {
        $headers = array('Content-Type' => 'application/json');

        if($authorized){
            if(!$this->accessToken){
                $this->authorize();
            }

            $headers['Authorization'] = "{$this->accessTokenType} {$this->accessToken}";
        }

        return $this->request($method, $url, $data, $headers);
    }

    public function request(string $method,string $url,array $data = null, $headers = array()) : array
    {
        $ch = \curl_init($this->baseLabelPrintterUrl . $url);

        \curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        \curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

        $headersPrepared = array_map(function($key, $value){
            return "{$key}: {$value}";
            }, array_keys($headers), array_values($headers));

        \curl_setopt($ch, CURLOPT_HTTPHEADER, $headersPrepared);

        if($method === 'POST'){
            if(!array_key_exists('Content-Type', $headers) || $headers['Content-Type'] === 'application/x-www-form-urlencoded'){
                \curl_setopt($ch, CURLOPT_POST, true);
                \curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            }elseif($headers['Content-Type'] === 'application/json') {
                \curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            }
        }

        $response = \curl_exec($ch);

        $parsed = \json_decode($response, true);

        if(!$parsed){
            // TODO throw error
            throw new \Exception('Can\'t connect to LabelPrinter');
        }

        return $parsed;
    }
}
