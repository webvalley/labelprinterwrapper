<?php

namespace LabelPrinter\Model\Shipper;

class Shipper
{
    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $phone;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var array
     */
    protected $services;

    /**
     * Shipper constructor.
     * @param string $code
     * @param string $name
     * @param string $phone
     * @param string $email
     * @param array $services
     */
    public function __construct(string $code, string $name, ?string $phone, ?string $email, array $services)
    {
        $this->code = $code;
        $this->name = $name;
        $this->phone = $phone;
        $this->email = $email;
        $this->services = $services;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return array
     */
    public function getServices(): array
    {
        return $this->services;
    }
}