<?php

namespace LabelPrinter\Model;

class SearchCriteria
{
    /**
     * @var ?integer
     */
    private $id;

    /**
     * @var array
     */
    private $variableSymbols = array();

    /**
     * @var array
     */
    private $orderNumbers = array();

    /**
     * @var ?\DateTime
     */
    private $shipmentDayFrom;

    /**
     * @var ?\DateTime
     */
    private $shipmentDayTo;

    /**
     * @var array
     */
    private $barcodes = array();

    /**
     * @var array
     */
    private $customExternalIds = array();

    public function serialize() : array
    {
        return array(
            'id' => $this->id,
            'variableSymbols' => $this->variableSymbols,
            'orderNumbers' => $this->orderNumbers,
            'shipmentDayFrom' => $this->shipmentDayFrom? $this->shipmentDayFrom->format('Y-m-d') : null,
            'shipmentDayTo' => $this->shipmentDayTo? $this->shipmentDayTo->format('Y-m-d') : null,
            'barcodes' => $this->barcodes,
            'customExternalIds' => $this->customExternalIds
        );
    }

    private function createSearchInDate(\DateTime $startDate, \DateTime $endDate)
    {
        return (new self())->setShipmentDayFrom($startDate)->setShipmentDayTo($endDate);
    }

    /**
     * @param mixed $id
     * @return SearchCriteria
     */
    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param array $variableSymbols
     * @return SearchCriteria
     */
    public function setVariableSymbols(array $variableSymbols): self
    {
        $this->variableSymbols = $variableSymbols;

        return $this;
    }

    /**
     * @param array $orderNumbers
     * @return SearchCriteria
     */
    public function setOrderNumbers(array $orderNumbers): self
    {
        $this->orderNumbers = $orderNumbers;

        return $this;
    }

    /**
     * @param mixed $shipmentDayFrom
     * @return SearchCriteria
     */
    public function setShipmentDayFrom(\DateTime $shipmentDayFrom): self
    {
        $this->shipmentDayFrom = $shipmentDayFrom;

        return $this;
    }

    /**
     * @param mixed $shipmentDayTo
     * @return SearchCriteria
     */
    public function setShipmentDayTo(\DateTime $shipmentDayTo): self
    {
        $this->shipmentDayTo = $shipmentDayTo;

        return $this;
    }

    /**
     * @param array $barcodes
     * @return SearchCriteria
     */
    public function setBarcodes(array $barcodes): self
    {
        $this->barcodes = $barcodes;

        return $this;
    }

    /**
     * @param array $customExternalIds
     * @return SearchCriteria
     */
    public function setCustomExternalIds(array $customExternalIds): self
    {
        $this->customExternalIds = $customExternalIds;

        return $this;
    }
}