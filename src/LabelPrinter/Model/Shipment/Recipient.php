<?php

namespace LabelPrinter\Model\Shipment;

class Recipient
{
    private $company;
    private $person;
    private $street;
    private $street2;
    private $city;
    private $postalCode;
    private $countryCode;
    private $ico;
    private $phone;
    private $email;

    /**
     * Recipient constructor.
     * @param $company
     * @param $street
     * @param $city
     * @param $postalCode
     * @param $countryCode
     */
    public function __construct(string $company,string $street,string $city,string $postalCode,string $countryCode)
    {
        $this->company = $company;
        $this->street = $street;
        $this->city = $city;
        $this->postalCode = $postalCode;
        $this->countryCode = $countryCode;
    }

    public function serialize() : array
    {
        $recipient = array(
            'company' => $this->company,
            'street' => $this->street,
            'city' => $this->city,
            'postalCode' => $this->postalCode,
            'countryCode' => $this->countryCode,
        );

        if($this->person){
            $recipient['person'] = $this->person;
        }

        if($this->street2){
            $recipient['street2'] = $this->street2;
        }

        if($this->ico){
            $recipient['ico'] = $this->ico;
        }

        if($this->phone){
            $recipient['phone'] = $this->phone;
        }

        if($this->email){
            $recipient['email'] = $this->email;
        }

        return $recipient;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     * @return Recipient
     */
    public function setCompany($company): Recipient
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @param mixed $person
     * @return Recipient
     */
    public function setPerson($person): Recipient
    {
        $this->person = $person;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param mixed $street
     * @return Recipient
     */
    public function setStreet($street): Recipient
    {
        $this->street = $street;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStreet2()
    {
        return $this->street2;
    }

    /**
     * @param mixed $street2
     * @return Recipient
     */
    public function setStreet2($street2): Recipient
    {
        $this->street2 = $street2;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return Recipient
     */
    public function setCity($city): Recipient
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param mixed $postalCode
     * @return Recipient
     */
    public function setPostalCode($postalCode): Recipient
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * @param mixed $countryCode
     * @return Recipient
     */
    public function setCountryCode($countryCode): Recipient
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIco()
    {
        return $this->ico;
    }

    /**
     * @param mixed $ico
     * @return Recipient
     */
    public function setIco($ico): Recipient
    {
        $this->ico = $ico;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     * @return Recipient
     */
    public function setPhone($phone): Recipient
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return Recipient
     */
    public function setEmail($email): Recipient
    {
        $this->email = $email;

        return $this;
    }
}