<?php


namespace LabelPrinter\Model\Shipment;


class Media
{
    public const TYPE_OTHER = 0;
    public const TYPE_INVOICE = 1;

    /**
     * @var string
     */
    protected $mediaType = self::TYPE_INVOICE;

    /**
     * @var string
     */
    protected $fileName;

    /**
     * @var string
     */
    protected $data;

    public function serialize() : array
    {
        return array(
            'mediaType' => $this->mediaType,
            'fileName' => $this->fileName,
            'data' => $this->data,
        );
    }

    /**
     * @return string
     */
    public function getMediaType(): string
    {
        return $this->mediaType;
    }

    /**
     * @param string $mediaType
     */
    public function setMediaType(string $mediaType): void
    {
        $this->mediaType = $mediaType;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     */
    public function setFileName(string $fileName): void
    {
        $this->fileName = $fileName;
    }

    /**
     * @return string
     */
    public function getData(): string
    {
        return $this->data;
    }

    /**
     * @param string $data
     */
    public function setData(string $data): void
    {
        $this->data = $data;
    }
}