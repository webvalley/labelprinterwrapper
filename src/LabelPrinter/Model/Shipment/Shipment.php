<?php

namespace LabelPrinter\Model\Shipment;

use LabelPrinter\LabelPrinter;

class Shipment
{
    public const STATE_NOT_SEND = 0;
    public const STATE_NOT_PRINTED = 1;
    public const STATE_PRINTED = 2;
    public const STATE_CANCELED = 3;
    public const STATE_PREPARED = 4;
    public const STATE_NOT_LOADED = 5;
    public const STATE_JOINED = 6;
    public const STATE_PRINTED_WITH_ERROR = 7;

    public const COD_PAYMENT_TYPE_CASH = 0;
    public const COD_PAYMENT_TYPE_CARD = 1;

    /**
     * @var LabelPrinter
     */
    private $labelPrinter;

    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string|null
     */
    private $processUser;

    /**
     * @var string
     */
    private $shipperCode;

    /**
     * @var string
     */
    private $serviceCode;

    /**
     * @var string
     */
    private $variableSymbol;

    /**
     * @var string
     */
    private $orderNumber;

    /**
     * @var boolean
     */
    private $paymentInAdvance;

    /**
     * @var double
     */
    private $price;

    /**
     * @var string
     */
    private $priceCurrency;

    /**
     * @var double
     */
    private $cod;

    /**
     * @var string
     */
    private $codCurrency;

    /**
     * @var integer
     */
    private $codPaymentType;

    /**
     * @var string
     */
    private $customExternalId;

    /**
     * @var string
     */
    private $description;

    /**
     * @var boolean
     */
    private $personalCollection;

    /**
     * @var string
     */
    private $deliveryPointId;

    /**
     * @var string
     */
    private $expeditionNote;

    /**
     * @var string
     */
    private $paletteManipulationType;

    /**
     * @var string
     */
    private $paletePickupType;

    /**
     * @var array
     */
    private $additionalServices;

    /**
     * @var Recipient
     */
    private $recipient;

    /**
     * @var PreferredSender
     */
    private $prefferedSender;

    /** @var array */
    private $parcels;

    /**
     * @var array
     */
    private $goods;

    /**
     * @var array
     */
    private $media;

    /**
     * @var string
     */
    private $labelFormat;

    private $lastState = self::STATE_NOT_SEND;

    /**
     * Shipment constructor.
     * @param $shipperCode
     * @param $serviceCode
     * @param $variableSymbol
     * @param $orderNumber
     * @param $price
     * @param $priceCurrency
     * @param $recipient
     * @param $parcels
     */
    public function __construct(LabelPrinter $labelPrinter)
    {
        $this->labelPrinter = $labelPrinter;

        $this->parcels = array();
        $this->goods = array();
        $this->additionalServices = array();
        $this->media = array();
    }

    public function serialize() : array
    {
        $serialized = array(
            'shipperCode' => $this->shipperCode,
            'serviceCode' => $this->serviceCode,
            'variableSymbol' => $this->variableSymbol,
            'orderNumber' => $this->orderNumber,
            'price' => $this->price,
            'priceCurrency' => $this->priceCurrency,
            'recipient' => $this->recipient->serialize(),
            'parcels' => [],
            'goods' => [],
            'media' => []
        );

        if($this->cod){
            $serialized['cod'] = $this->cod;
            $serialized['codCurrency'] = $this->codCurrency;
        }

        /** @var Parcel $parcel */
        foreach($this->parcels as $parcel){
            $serialized['parcels'][] = $parcel->serialize();
        }

        /** @var Goods $goods */
        foreach($this->goods as $goods){
            $serialized['goods'][] = $goods->serialize();
        }

        if($this->id) {
            $serialized['id'] = $this->id;
        }

        if($this->processUser){
            $serialized['processUser'] = $this->processUser;
        }

        /** @var Media $media */
        foreach($this->media as $media) {
            $serialized['media'][] = $media->serialize();
        }

        if($this->labelFormat){
            $serialized['labels'] = array('format' => $this->labelFormat);
        }

        // Balikovna Point Id
        if($this->deliveryPointId) {
            $serialized['deliveryPointId'] = $this->deliveryPointId;
        }

        return $serialized;
    }

    public function getTrackingCodes() : array
    {
        $trackingCodes = array();

        /** @var Parcel $parcel */
        foreach($this->parcels as $parcel)
        {
            if($parcel->getTrackingCode()){
                $trackingCodes[] = $parcel->getTrackingCode();
            }
        }

        return $trackingCodes;
    }

    public function deserialize(array $data) : Shipment
    {
        $this->id = $data[0]['id'];
        $this->lastState = $data[0]['state']['code'];

        for($i = 0, $iMax = count($data[0]['parcels']); $i < $iMax; $i++){
            if(isset($this->parcels[0])){
                $parcel = $this->parcels[0];
            }else{
                $parcel = new Parcel();
                $this->parcels[] = $parcel;
            }

            $parcel->setBarcode($data[0]['parcels'][$i]['barcode']);
            $parcel->setWeight($data[0]['parcels'][$i]['weight']);
            $parcel->setWidth($data[0]['parcels'][$i]['width']);
            $parcel->setHeight($data[0]['parcels'][$i]['height']);
            $parcel->setDepth($data[0]['parcels'][$i]['depth']);
            $parcel->setVolume($data[0]['parcels'][$i]['volume']);
            $parcel->setWrapCode($data[0]['parcels'][$i]['wrapCode']);
            $parcel->setTrackingCode($data[0]['parcels'][$i]['trackingNumber']);
        }

        return $this;
    }

    public function save()
    {
        $this->labelPrinter->sendShipment($this);
    }

    public function addParcel(Parcel $parcel)
    {
        $this->parcels[] = $parcel;
    }

    public function addGoods(Goods $goods)
    {
        $this->goods[] = $goods;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getProcessUser(): ?string
    {
        return $this->processUser;
    }

    /**
     * @param string|null $processUser
     * @return Shipment
     */
    public function setProcessUser(?string $processUser): self
    {
        $this->processUser = $processUser;

        return $this;
    }

    /**
     * @return string
     */
    public function getShipperCode(): string
    {
        return $this->shipperCode;
    }

    /**
     * @param string $shipperCode
     * @return Shipment
     */
    public function setShipperCode(string $shipperCode): self
    {
        $this->shipperCode = $shipperCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getServiceCode(): string
    {
        return $this->serviceCode;
    }

    /**
     * @param string $serviceCode
     * @return Shipment
     */
    public function setServiceCode(string $serviceCode): self
    {
        $this->serviceCode = $serviceCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getVariableSymbol(): string
    {
        return $this->variableSymbol;
    }

    /**
     * @param string $variableSymbol
     * @return Shipment
     */
    public function setVariableSymbol(string $variableSymbol): self
    {
        $this->variableSymbol = $variableSymbol;

        return $this;
    }

    /**
     * @return string
     */
    public function getOrderNumber(): string
    {
        return $this->orderNumber;
    }

    /**
     * @param string $orderNumber
     * @return Shipment
     */
    public function setOrderNumber(string $orderNumber): self
    {
        $this->orderNumber = $orderNumber;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPaymentInAdvance(): bool
    {
        return $this->paymentInAdvance;
    }

    /**
     * @param bool $paymentInAdvance
     * @return Shipment
     */
    public function setPaymentInAdvance(bool $paymentInAdvance): self
    {
        $this->paymentInAdvance = $paymentInAdvance;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return Shipment
     */
    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return string
     */
    public function getPriceCurrency(): string
    {
        return $this->priceCurrency;
    }

    /**
     * @param string $priceCurrency
     * @return Shipment
     */
    public function setPriceCurrency(string $priceCurrency): self
    {
        $this->priceCurrency = $priceCurrency;

        return $this;
    }

    /**
     * @return float
     */
    public function getCod(): float
    {
        return $this->cod;
    }

    /**
     * @param float $cod
     * @return Shipment
     */
    public function setCod(float $cod): self
    {
        $this->cod = $cod;

        return $this;
    }

    /**
     * @return string
     */
    public function getCodCurrency(): string
    {
        return $this->codCurrency;
    }

    /**
     * @param string $codCurrency
     * @return Shipment
     */
    public function setCodCurrency(string $codCurrency): self
    {
        $this->codCurrency = $codCurrency;

        return $this;
    }

    /**
     * @return int
     */
    public function getCodPaymentType(): int
    {
        return $this->codPaymentType;
    }

    /**
     * @param int $codPaymentType
     * @return Shipment
     */
    public function setCodPaymentType(int $codPaymentType): self
    {
        $this->codPaymentType = $codPaymentType;

        return $this;
    }

    /**
     * @return string
     */
    public function getCustomExternalId(): string
    {
        return $this->customExternalId;
    }

    /**
     * @param string $customExternalId
     * @return Shipment
     */
    public function setCustomExternalId(string $customExternalId): self
    {
        $this->customExternalId = $customExternalId;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Shipment
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPersonalCollection(): bool
    {
        return $this->personalCollection;
    }

    /**
     * @param bool $personalCollection
     * @return Shipment
     */
    public function setPersonalCollection(bool $personalCollection): self
    {
        $this->personalCollection = $personalCollection;

        return $this;
    }

    /**
     * @return string
     */
    public function getDeliveryPointId(): string
    {
        return $this->deliveryPointId;
    }

    /**
     * @param string $deliveryPointId
     * @return Shipment
     */
    public function setDeliveryPointId(string $deliveryPointId): self
    {
        $this->deliveryPointId = $deliveryPointId;

        return $this;
    }

    /**
     * @return string
     */
    public function getExpeditionNote(): string
    {
        return $this->expeditionNote;
    }

    /**
     * @param string $expeditionNote
     * @return Shipment
     */
    public function setExpeditionNote(string $expeditionNote): self
    {
        $this->expeditionNote = $expeditionNote;

        return $this;
    }

    /**
     * @return string
     */
    public function getPaletteManipulationType(): string
    {
        return $this->paletteManipulationType;
    }

    /**
     * @param string $paletteManipulationType
     * @return Shipment
     */
    public function setPaletteManipulationType(string $paletteManipulationType): self
    {
        $this->paletteManipulationType = $paletteManipulationType;

        return $this;
    }

    /**
     * @return string
     */
    public function getPaletePickupType(): string
    {
        return $this->paletePickupType;
    }

    /**
     * @param string $paletePickupType
     * @return Shipment
     */
    public function setPaletePickupType(string $paletePickupType): self
    {
        $this->paletePickupType = $paletePickupType;

        return $this;
    }

    /**
     * @return array
     */
    public function getAdditionalServices(): array
    {
        return $this->additionalServices;
    }

    /**
     * @param array $additionalServices
     * @return Shipment
     */
    public function setAdditionalServices(array $additionalServices): self
    {
        $this->additionalServices = $additionalServices;

        return $this;
    }

    /**
     * @return Recipient
     */
    public function getRecipient(): Recipient
    {
        return $this->recipient;
    }

    /**
     * @param Recipient $recipient
     * @return Shipment
     */
    public function setRecipient(Recipient $recipient): self
    {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * @return PreferredSender
     */
    public function getPrefferedSender(): PreferredSender
    {
        return $this->prefferedSender;
    }

    /**
     * @param PreferredSender $prefferedSender
     * @return Shipment
     */
    public function setPrefferedSender(PreferredSender $prefferedSender): self
    {
        $this->prefferedSender = $prefferedSender;

        return $this;
    }

    /**
     * @return array
     */
    public function getParcels(): array
    {
        return $this->parcels;
    }

    /**
     * @param array $parcels
     * @return Shipment
     */
    public function setParcels(array $parcels): self
    {
        $this->parcels = $parcels;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLabelFormat()
    {
        return $this->labelFormat;
    }

    /**
     * @param mixed $labelFormat
     * @return Shipment
     */
    public function setLabelFormat($labelFormat): self
    {
        $this->labelFormat = $labelFormat;

        return $this;
    }

    /**
     * @return array
     */
    public function getMedia(): array
    {
        return $this->media;
    }

    /**
     * @param Media $media
     */
    public function addMedia(Media $media): void
    {
        $this->media[] = $media;
    }

    public function getStateCode() : int
    {
        return $this->lastState;
    }
}
