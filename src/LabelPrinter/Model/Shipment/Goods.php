<?php
/**
 * Created by PhpStorm.
 * User: Ondřej
 * Date: 05.04.2019
 * Time: 11:30
 */

namespace LabelPrinter\Model\Shipment;


class Goods
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $barcode;

    /**
     * @var string
     */
    protected $location;

    /**
     * @var int
     */
    protected $quantity;

    /**
     * @var integer
     */
    protected $ratio;

    /**
     * @var string
     */
    protected $unitCode;

    /**
     * @var float
     */
    protected $unitPrice;

    /**
     * @var string
     */
    protected $currencyCode;

    /**
     * @var string
     */
    protected $naftaTariffCode;

    /**
     * @var string
     */
    protected $countryCode;

    /**
     * @var array
     */
    protected $alternatives = array();

    /**
     * Goods constructor.
     * @param $barcode
     * @param $quantity
     * @param $unitCode
     */
    public function __construct(string $barcode,int $quantity = 1,string $unitCode = 'ks')
    {
        $this->barcode = $barcode;
        $this->quantity = $quantity;
        $this->unitCode = $unitCode;
    }

    public function serialize() : array
    {
        $serialized = array(
            'barcode' => $this->barcode,
            'quantity' => $this->quantity,
            'unitCode' => $this->unitCode
        );

        if($this->name){
            $serialized['name'] = $this->name;
        }

        if($this->location){
            $serialized['location'] = $this->location;
        }

        if($this->unitPrice){
            $serialized['unitPrice'] = $this->unitPrice;
        }

        if($this->currencyCode){
            $serialized['currencyCode'] = $this->currencyCode;
        }

        if($this->naftaTariffCode){
            $serialized['naftaTariffCode'] = $this->naftaTariffCode;
        }

        if($this->countryCode){
            $serialized['countryCode'] = $this->countryCode;
        }

        if(count($this->alternatives)){
            $serialized['alternatives'] = array();
            /** @var GoodsAlternative $alternative */
            foreach($this->alternatives as $alternative){
                $serialized['alternatives'][] = $alternative->serialize();
            }
        }

        return $serialized;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getBarcode(): string
    {
        return $this->barcode;
    }

    /**
     * @param string $barcode
     */
    public function setBarcode(string $barcode): void
    {
        $this->barcode = $barcode;
    }

    /**
     * @return string
     */
    public function getLocation(): string
    {
        return $this->location;
    }

    /**
     * @param string $location
     */
    public function setLocation(string $location): void
    {
        $this->location = $location;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return int
     */
    public function getRatio(): int
    {
        return $this->ratio;
    }

    /**
     * @param int $ratio
     */
    public function setRatio(int $ratio): void
    {
        $this->ratio = $ratio;
    }

    /**
     * @return string
     */
    public function getUnitCode(): string
    {
        return $this->unitCode;
    }

    /**
     * @param string $unitCode
     */
    public function setUnitCode(string $unitCode): void
    {
        $this->unitCode = $unitCode;
    }

    /**
     * @return float
     */
    public function getUnitPrice(): float
    {
        return $this->unitPrice;
    }

    /**
     * @param float $unitPrice
     */
    public function setUnitPrice(float $unitPrice): void
    {
        $this->unitPrice = $unitPrice;
    }

    /**
     * @return string
     */
    public function getCurrencyCode(): string
    {
        return $this->currencyCode;
    }

    /**
     * @param string $currencyCode
     */
    public function setCurrencyCode(string $currencyCode): void
    {
        $this->currencyCode = $currencyCode;
    }

    /**
     * @return string
     */
    public function getNaftaTariffCode(): string
    {
        return $this->naftaTariffCode;
    }

    /**
     * @param string $naftaTariffCode
     */
    public function setNaftaTariffCode(string $naftaTariffCode): void
    {
        $this->naftaTariffCode = $naftaTariffCode;
    }

    /**
     * @return string
     */
    public function getCountryCode(): string
    {
        return $this->countryCode;
    }

    /**
     * @param string $countryCode
     */
    public function setCountryCode(string $countryCode): void
    {
        $this->countryCode = $countryCode;
    }

    /**
     * @return array
     */
    public function getAlternatives(): array
    {
        return $this->alternatives;
    }

    /**
     * @param array $alternatives
     */
    public function setAlternatives(array $alternatives): void
    {
        $this->alternatives = $alternatives;
    }
}