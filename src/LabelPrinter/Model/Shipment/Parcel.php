<?php

namespace LabelPrinter\Model\Shipment;

class Parcel
{
    /**
     * @var string
     */
    private $barcode;

    /**
     * @var ?double
     */
    private $weight;

    /**
     * @var ?double
     */
    private $width;

    /**
     * @var ?double
     */
    private $height;

    /**
     * @var ?double
     */
    private $depth;

    /**
     * @var ?double
     */
    private $volume;

    /**
     * @var string
     */
    private $wrapCode;

    /**
     * @var string
     */
    private $trackingCode;

    public function serialize() : array
    {
        $parcel = array();

        if($this->barcode){
            $parcel['barcode'] = $this->barcode;
        }

        if($this->weight){
            $parcel['weight'] = $this->weight;
        }

        if($this->width){
            $parcel['width'] = $this->width;
        }

        if($this->height){
            $parcel['height'] = $this->height;
        }

        if($this->depth){
            $parcel['depth'] = $this->depth;
        }

        if($this->volume){
            $parcel['volume'] = $this->volume;
        }

        if($this->wrapCode){
            $parcel['wrapCode'] = $this->wrapCode;
        }

        return $parcel;
    }

    /**
     * @return string
     */
    public function getBarcode(): ?string
    {
        return $this->barcode;
    }

    /**
     * @param string $barcode
     * @return Parcel
     */
    public function setBarcode(?string $barcode): self
    {
        $this->barcode = $barcode;

        return $this;
    }

    /**
     * @return float
     */
    public function getWeight(): ?float
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     * @return Parcel
     */
    public function setWeight(?float $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * @return float
     */
    public function getHeight(): ?float
    {
        return $this->height;
    }

    /**
     * @param float $height
     * @return Parcel
     */
    public function setHeight(?float $height): self
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @return float
     */
    public function getDepth(): ?float
    {
        return $this->depth;
    }

    /**
     * @param float $depth
     * @return Parcel
     */
    public function setDepth(?float $depth): self
    {
        $this->depth = $depth;

        return $this;
    }

    /**
     * @return float
     */
    public function getVolume(): ?float
    {
        return $this->volume;
    }

    /**
     * @param float $volume
     * @return Parcel
     */
    public function setVolume(?float $volume): self
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * @return string
     */
    public function getWrapCode(): ?string
    {
        return $this->wrapCode;
    }

    /**
     * @param string $wrapCode
     * @return Parcel
     */
    public function setWrapCode(?string $wrapCode): self
    {
        $this->wrapCode = $wrapCode;

        return $this;
    }

    /**
     * @return float
     */
    public function getWidth(): ?float
    {
        return $this->width;
    }

    /**
     * @param float $width
     */
    public function setWidth(?float $width): void
    {
        $this->width = $width;
    }

    /**
     * @return string
     */
    public function getTrackingCode(): ?string
    {
        return $this->trackingCode;
    }

    /**
     * @param string $trackingCode
     */
    public function setTrackingCode(?string $trackingCode): void
    {
        $this->trackingCode = $trackingCode;
    }
}