<?php

namespace LabelPrinter\Model\Shipment;

class PreferredSender
{
    private $company;
    private $street;
    private $city;
    private $postalCode;
    private $countryCode;
    private $phone;
    private $email;
    private $logo;

    /**
     * PreferredSender constructor.
     * @param $company
     * @param $street
     * @param $city
     * @param $postalCode
     * @param $countryCode
     */
    public function __construct(string $company, string $street, string $city, string $postalCode, string $countryCode)
    {
        $this->company = $company;
        $this->street = $street;
        $this->city = $city;
        $this->postalCode = $postalCode;
        $this->countryCode = $countryCode;
    }

    public function serialize() : array
    {
        $serialized = array(
            'company' => $this->company,
            'street' => $this->street,
            'city' => $this->city,
            'postalCode' => $this->postalCode,
            'countryCode' => $this->countryCode,
        );

        if($this->phone){
            $serialized['phone'] = $this->phone;
        }

        if($this->email){
            $serialized['email'] = $this->email;
        }

        if($this->logo){
            $serialized['logo'] = $this->logo;
        }

        return $serialized;
    }

    /**
     * @return string
     */
    public function getCompany(): string
    {
        return $this->company;
    }

    /**
     * @param string $company
     * @return PreferredSender
     */
    public function setCompany(string $company): self
    {
        $this->company = $company;
        
        return $this;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @param string $street
     * @return PreferredSender
     */
    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return PreferredSender
     */
    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    /**
     * @param string $postalCode
     * @return PreferredSender
     */
    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getCountryCode(): string
    {
        return $this->countryCode;
    }

    /**
     * @param string $countryCode
     * @return PreferredSender
     */
    public function setCountryCode(string $countryCode): self
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     * @return PreferredSender
     */
    public function setPhone($phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return PreferredSender
     */
    public function setEmail($email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param mixed $logo
     * @return PreferredSender
     */
    public function setLogo($logo): self
    {
        $this->logo = $logo;

        return $this;
    }
}