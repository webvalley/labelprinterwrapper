<?php
/**
 * Created by PhpStorm.
 * User: Ondřej
 * Date: 05.04.2019
 * Time: 11:31
 */

namespace LabelPrinter\Model\Shipment;


class GoodsAlternative
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string string
     */
    protected $barcode;

    /**
     * @var string
     */
    protected $location;

    /**
     * @var float
     */
    protected $ratio;

    /**
     * GoodsAlternative constructor.
     * @param $barcode
     */
    public function __construct(string $barcode)
    {
        $this->barcode = $barcode;
    }

    public function serialize() : array
    {
        $serialized = array(
            'barcode' => $this->barcode
        );

        if($this->name){
            $serialized['name'] = $this->name;
        }

        if($this->location){
            $serialized['location'] = $this->location;
        }

        if($this->ratio){
            $serialized['ratio'] = $this->ratio;
        }

        return $serialized;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getBarcode(): string
    {
        return $this->barcode;
    }

    /**
     * @param string $barcode
     */
    public function setBarcode(string $barcode): void
    {
        $this->barcode = $barcode;
    }

    /**
     * @return string
     */
    public function getLocation(): string
    {
        return $this->location;
    }

    /**
     * @param string $location
     */
    public function setLocation(string $location): void
    {
        $this->location = $location;
    }

    /**
     * @return float
     */
    public function getRatio(): float
    {
        return $this->ratio;
    }

    /**
     * @param float $ratio
     */
    public function setRatio(float $ratio): void
    {
        $this->ratio = $ratio;
    }
}